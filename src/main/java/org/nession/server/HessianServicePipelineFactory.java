
package org.nession.server;

import java.util.concurrent.Executor;

import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.nession.channelhandler.HessianServiceInovker;
import org.nession.channelhandler.InputStreamDecoder;
import org.nession.channelhandler.OutputStreamEncoder;

import com.caucho.hessian.server.HessianSkeleton;

/**
 * HessianServicePipelineFactory
 * 
 * @author 君枫
 * @version 1.0
 * @since 2013年12月22日 下午4:09:31
 */
public class HessianServicePipelineFactory implements ChannelPipelineFactory
{

	private final HessianSkeleton hessianSkeleton;
	private final Executor executor;

	/**
	 * @param hessianSkeleton
	 */
	public HessianServicePipelineFactory(HessianSkeleton hessianSkeleton, Executor executor)
	{
		this.hessianSkeleton = hessianSkeleton;
		this.executor = executor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.jboss.netty.channel.ChannelPipelineFactory#getPipeline()
	 */
	@Override
	public ChannelPipeline getPipeline() throws Exception
	{
		ChannelPipeline pipeline = Channels.pipeline();
		pipeline.addLast("InputStreamDecoder", new InputStreamDecoder(executor));// 将Channel转化为Stream使用
		pipeline.addLast("OutputStreamEncoder", new OutputStreamEncoder());// 将Channel转化为Stream使用
		// pipeline.addLast("", new ExecutionHandler(new
		// OrderedMemoryAwareThreadPoolExecutor(16, 1048576, 1048576)));
		pipeline.addLast("invokerhandler", new HessianServiceInovker());
		// 将hessianSkeleton注入到HessianServiceInovker中执行
		pipeline.getContext(HessianServiceInovker.class).setAttachment(hessianSkeleton);
		return pipeline;
	}
}
