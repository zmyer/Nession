
package org.nession.channelhandler;

import java.net.InetSocketAddress;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelHandler;
import org.nession.io.InputStreamBuffer;
import org.nession.server.AbstractHessianServiceExporter;
import org.nession.server.ServiceStatus;

import com.caucho.hessian.io.HessianInput;
import com.caucho.hessian.io.HessianOutput;
import com.caucho.hessian.server.HessianSkeleton;
import com.caucho.services.server.ServiceContext;

/**
 * hessian服务执行handler
 * 
 * <pre>
 * V1.1(2014-4-11)：
 *   1.{@link #messageReceived(ChannelHandlerContext, MessageEvent)}增加while循环执行代码，兼容长连接
 * 	
 * 
 * @author 君枫
 * @version 1.1
 * @since 2013年12月22日 下午4:34:30
 */
public class HessianServiceInovker extends SimpleChannelHandler
{

	private static Log log = LogFactory.getLog(HessianServiceInovker.class);
	private HessianSkeleton hessianSkeleton;
	public static final String ID_FROM = "__from";
	/**
	 * 未完成请求数
	 */
	private static AtomicInteger currentRequstNumber = new AtomicInteger(0);
	/**
	 * 异常请求数
	 */
	private static AtomicInteger exceptionRequestNumber = new AtomicInteger(0);
	/**
	 * 成功请求数
	 */
	private static AtomicInteger sucessRequestNumber = new AtomicInteger(0);
	static
	{
		new Thread(new Runnable()
		{

			private void threadSleep()
			{
				try
				{
					Thread.sleep(10000);
				}
				catch (InterruptedException e)
				{
				}
			}

			@Override
			public void run()
			{
				Collection<AbstractHessianServiceExporter> services = null;
				while (true)
				{
					services = AbstractHessianServiceExporter.getAllService();
					log.info("#Hessian存活对外暴露服务：");
					for (AbstractHessianServiceExporter service : services)
					{
						if (service != null && service.getServiceStatus() == ServiceStatus.Started)
						{
							log.info("	[" + service.getServiceInterface().getName() + "][" + service.getPort() + "]");
						}
					}
					log.info("#未完成请求数：" + currentRequstNumber.get());
					log.info("#异常请求数：" + exceptionRequestNumber.get());
					log.info("#成功请求数：" + sucessRequestNumber.get());
					threadSleep();
				}
			}
		}).start();
	}

	@SuppressWarnings("unchecked")
	static <T> T cast(Object obj)
	{
		return (T) obj;
	}

	/**
	 * 从 {@link Channel}获取远程IP地址信息
	 * 
	 * @param channel
	 * @return
	 */
	static String retrieveRemoteIPFrom(Channel channel)
	{
		if (channel != null && channel.getRemoteAddress() != null)
		{
			if (channel.getRemoteAddress() instanceof InetSocketAddress)
			{
				return ((InetSocketAddress) channel.getRemoteAddress()).getAddress().getHostAddress();
			}
		}
		return null;
	}

	/**
	 * 打印当前方法的调用栈信息
	 */
	public static void printCallStack()
	{
		/**
		 * 最顶上的为Thread.currentThread().getStackTrace()<br>
		 * 其次是printCallStack
		 */
		StackTraceElement[] callStack = Thread.currentThread().getStackTrace();
		StackTraceElement currentElement;
		for (int i = 2; i < callStack.length; i++)
		{
			currentElement = callStack[i];
			log.info(currentElement.getClassName() + "." + currentElement.getMethodName() + "(" + currentElement.getFileName()
					+ ":" + currentElement.getLineNumber() + ")");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jboss.netty.channel.SimpleChannelHandler#messageReceived(org.jboss.netty.channel
	 * .ChannelHandlerContext, org.jboss.netty.channel.MessageEvent)
	 */
	@Override
	public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception
	{
		ServiceContext.begin(null, null, null);
		ServiceContext.getContext().addHeader(ID_FROM, retrieveRemoteIPFrom(e.getChannel()));
		InputStreamBuffer inputStreamBuffer = cast(e.getMessage());
		/**
		 * 采用while循环，兼容长连接
		 */
		// 可能连接为长连接，判断是否结束
		boolean isEnd = false;
		while (!isEnd)
		{
			try
			{
				getHessianSkeleton().invoke(new HessianInput(inputStreamBuffer),
						new HessianOutput(OutputStreamEncoder.getOutputStream(ctx)));
			}
			catch (Exception e1)
			{
				// TODO 需要准确判断异常类型，来判断是否请求结束
				isEnd = true;
			}
		}
		sucessRequestNumber.incrementAndGet();
		ServiceContext.end();
	}

	/*
	 * 客户端关闭channel触发服务端对应的channel关闭
	 * 
	 * @see
	 * org.jboss.netty.channel.SimpleChannelHandler#channelConnected(org.jboss.netty.channel
	 * .ChannelHandlerContext, org.jboss.netty.channel.ChannelStateEvent)
	 */
	@Override
	public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception
	{
		currentRequstNumber.incrementAndGet();
		if (getHessianSkeleton() == null)
		{
			HessianSkeleton skeleton = cast(ctx.getAttachment());
			setHessianSkeleton(skeleton);
		}
		super.channelConnected(ctx, e);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jboss.netty.channel.SimpleChannelHandler#exceptionCaught(org.jboss.netty.channel
	 * .ChannelHandlerContext, org.jboss.netty.channel.ExceptionEvent)
	 */
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception
	{
		log.error("发生异常:" + e.getCause().getMessage(), e.getCause());
		exceptionRequestNumber.incrementAndGet();
		e.getChannel().close();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jboss.netty.channel.SimpleChannelHandler#channelDisconnected(org.jboss.netty
	 * .channel.ChannelHandlerContext, org.jboss.netty.channel.ChannelStateEvent)
	 */
	@Override
	public void channelDisconnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception
	{
		// printCallStack();
		log.debug("断开连接[" + e.getChannel().getRemoteAddress() + "," + e.getChannel().getLocalAddress() + "]");
		currentRequstNumber.decrementAndGet();
		super.channelDisconnected(ctx, e);
	}

	/**
	 * @return the hessianSkeleton
	 */
	public HessianSkeleton getHessianSkeleton()
	{
		return hessianSkeleton;
	}

	/**
	 * @param hessianSkeleton
	 *            the hessianSkeleton to set
	 */
	private void setHessianSkeleton(HessianSkeleton hessianSkeleton)
	{
		this.hessianSkeleton = hessianSkeleton;
	}
}
