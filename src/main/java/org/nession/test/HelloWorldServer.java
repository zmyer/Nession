
package org.nession.test;

import org.nession.server.AbstractHessianServiceExporter;
import org.nession.server.HessianServiceExporter;
import org.nession.service.HelloWorld;
import org.nession.service.IHelloWorld;

/**
 * @author 君枫
 * @version 1.0
 * @since 2014-9-5 上午11:19:30
 */
public class HelloWorldServer
{

	public static void main(String[] args)
	{
		AbstractHessianServiceExporter serviceExporter = new HessianServiceExporter();
		serviceExporter.setPort(1234);
		serviceExporter.setServiceInterface(IHelloWorld.class);
		serviceExporter.setService(new HelloWorld());
		serviceExporter.startUp();
	}
}
