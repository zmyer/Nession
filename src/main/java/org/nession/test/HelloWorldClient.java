package org.nession.test;

import org.nession.client.HessianProxyFactory;
import org.nession.service.IHelloWorld;

/**
 * @author 君枫
 * @version 1.0
 * @since 2014-9-5 上午11:35:54
 */
public class HelloWorldClient {

    public static void main(String[] args) {
        HessianProxyFactory<IHelloWorld> hessianProxyFactory = new HessianProxyFactory<IHelloWorld>(IHelloWorld.class);
        IHelloWorld s = hessianProxyFactory.create("localhost", 1234, null);
        System.out.println(s.hello("访客" + System.currentTimeMillis()));
    }
}
